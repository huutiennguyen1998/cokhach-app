import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('username') uname;
  @ViewChild('password') password;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }

  doLogin() {
    this.navCtrl.setRoot('MenuPage');
  }

  doLoginGoogle() {
    this.navCtrl.push('LoginGooglePage');
  }

  pushRegister() {
    this.navCtrl.push('RegisterPage');
  }
  
  pushForgotPW() {
    this.navCtrl.push('ForgotPasswordPage');
  }

}
