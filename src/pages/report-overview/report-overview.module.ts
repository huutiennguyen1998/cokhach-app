import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportOverviewPage } from './report-overview';

@NgModule({
  declarations: [
    ReportOverviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportOverviewPage),
  ],
})
export class ReportOverviewPageModule {}
