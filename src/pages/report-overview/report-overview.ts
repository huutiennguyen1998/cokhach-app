import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportOverviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report-overview',
  templateUrl: 'report-overview.html',
})
export class ReportOverviewPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  customer: string;

  ionViewWillEnter() {
    this.customer = "online";
  }

}
