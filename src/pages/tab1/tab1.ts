import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tab1',
  templateUrl: 'tab1.html',
})
export class Tab1Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  itemSelected(item: string) {
    this.navCtrl.push('ReportOverviewPage');
  }

  items = [
    'cokhach.com',
    'chanclickao.com.vn',
    'fff.com.vn'
  ];

}
